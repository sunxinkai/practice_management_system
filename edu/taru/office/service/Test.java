package edu.taru.office.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class Test{
	private static ThreadLocal<Connection> local=new ThreadLocal<Connection>();
	private static Properties properties=new Properties();
	/*
	 * 加载文档
	 */
	static {
		try {
			properties.load(Test.class.getClassLoader().getResourceAsStream("jbdc.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/*
	 * 加载驱动
	 */
	static{
		try {
			Class.forName(properties.getProperty("driver"));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	/*
	 * 建立连接
	 */
	public static Connection OpenConnection(){
		Connection conn=local.get();
		try {
			if(conn==null|| conn.isClosed()){
			conn=DriverManager.getConnection(properties.getProperty("url"),properties.getProperty("user"),properties.getProperty("password"));
			local.set(conn);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	/*
	 * 查询
	 */
	public static ResultSet ToSelect1(String sql,Object ... pareams){
		Connection conn=Test.OpenConnection();
		ResultSet rs=null;
		PreparedStatement pst=null;
		try {
			pst=conn.prepareStatement(sql);
			if(pareams!=null){
				for(int i=0;i<pareams.length;i++){
					pst.setObject(i+1, pareams[i]);
				}
			}
			rs=pst.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}
	/*
	 * 查询1
	 */
	public static <T>List<T> select1(String sql,RowHandlerMapper<T> handlerMapper,Object ... pareams){
		Connection conn=Test.OpenConnection();
		ResultSet rs=null;
		PreparedStatement pst=null;
		List<T> rows=null;
		try {
			pst=conn.prepareStatement(sql);
			if(pareams!=null){
				for(int i=0;i<pareams.length;i++){
					pst.setObject(i+1, pareams[i]);
				}	
				}
			rs=pst.executeQuery();
			rows=handlerMapper.maping(rs);//数据库解析完毕，==调用了子类 Mapping方法
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(pst!=null){
				try {
					pst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return rows;
	}
	//创建接口，储存数据
	interface RowHandlerMapper<T>{
		public List<T> maping(ResultSet rs);
	}
	/*
	 * 查询2
	 */
	public static <T>List<HashMap<String, Object>> select2(String sql,Object ... pareams){
		Connection conn=Test.OpenConnection();
		ResultSet rs=null;
		PreparedStatement pst=null;
		List<HashMap<String,Object>> rows=new ArrayList<HashMap<String,Object>>();
		try {
			pst=conn.prepareStatement(sql);
			if(pareams!=null){
				for(int i=0;i<pareams.length;i++){
					pst.setObject(i+1, pareams[i]);
				}	
				}
			rs=pst.executeQuery();
			ResultSetMetaData rsmata =rs.getMetaData();//数据库解析完毕，==调用了子类 Mapping方法
			int length=rsmata.getColumnCount();
			while(rs.next()){
				HashMap<String,Object> map=new HashMap<String,Object>();
				for(int i=0;i<length;i++){
					String columnLabel=rsmata.getColumnLabel(i+1);
					map.put(columnLabel,rs.getObject(columnLabel));
				}
				rows.add(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("执行查询异常",e);
		}finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(pst!=null){
				try {
					pst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return rows;
	}
	
	
	/*
	 * 更新
	 */
	
	public static int ToUpdate(String sql,Object ... pareams){
		int row=0;
		Connection conn=Test.OpenConnection();
		PreparedStatement pst=null;
		try {
			pst=conn.prepareStatement(sql);
			if(pareams!=null){
				for(int i=0;i<pareams.length;i++){
					pst.setObject(i+1, pareams[i]);
				}
			}
			row=pst.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(pst!=null){
				try {
					pst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return row;
	}
	
	/*
	 * 关闭连接
	 */
	public void Toclose(){
		Connection conn=local.get();
		if(conn!=null){
			try {
				conn.close();
				local.remove();
				conn=null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}