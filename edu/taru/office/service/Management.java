package edu.taru.office.service;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import edu.taru.office.model.User;
public class  Management{
		public static  void main(String[]args) {
			Management m=new Management();
		}
		
		/*
		 * 
		 * 登录方法
		 * 
		 */
		public  User login(String name,String password){
			//从文件中读取每个数据 进行判断，如果匹配登陆成功，反之失败
			User user=null;
			String sql="select *from hr_user where name=? and password=?";	
			try {
				List<HashMap<String,Object>> maps=Test.select2(sql,name,password);
				if(maps!=null && maps.size()>0){
					HashMap<String,Object> map=maps.get(0);
					user =new User();
					user.setId(map.get("id")==null?null:map.get("id").toString());
					user.setName(map.get("name")==null?null:map.get("name").toString());
					user.setPassword(map.get("password")==null?null:map.get("password").toString());
					user.setSex(map.get("sex")==null?null:map.get("sex").toString());
					user.setEmail(map.get("email")==null?null:map.get("email").toString());	
				}
			} catch (Exception e) {
			  throw new RuntimeException(e+"用户不匹配");	 
			}
			return user;
		}
		
		/*
		 * 
		 * 注册方法
		 * 
		 */
		public int registered(User n){
			//从文件中读取每个数据 进行判断，如果匹配登陆成功，反之失败
			int a=0;
			User user=null;
			String sql="select *from hr_user where name=?";	
			try {
				List<HashMap<String,Object>> maps=Test.select2(sql,n.getName());
				if(maps!=null&&maps.size()>0){
					//判断用户名是否注册过
					return a;
				}else {
				//没有注册过，插入一条用户数据
				String sql2="Insert into hr_user(id,name,password,age,sex,email,tel,adress)values(null,?,?,?,?,?,?,?)";	
				a=Test.ToUpdate(sql2,n.getName(),n.getPassword(),n.getAge(),n.getSex(),n.getEmail(),n.getTel(),n.getAdress());
				return a=1;
				}
			} catch (Exception e) {
				throw new RuntimeException(e+"用户名已存在");
			}	
			}
		//修改密码
				public void remove(String name,String password,String password1){ 
					String sql="UpDate hr_user set password=? where name=?";
					if(password.equals(password1)) {
						try{
							Test.ToUpdate(sql,password,name);
						} catch (Exception e) {
							  throw new RuntimeException(e+"密码不匹配");	 
						}
					}
					
				}
				//查看登录用户个人信息 
		public User See(String name){
			User user=null;
			String sql="select *from hr_user where name=?";	
			try {
				List<HashMap<String,Object>> maps=Test.select2(sql,name);
				if(maps!=null && maps.size()>0){
					HashMap<String,Object> map=maps.get(0);
					user =new User();
					user.setId(map.get("id")==null?null:map.get("id").toString());
					user.setName(map.get("name")==null?null:map.get("name").toString());
					user.setPassword(map.get("password")==null?null:map.get("password").toString());
					user.setSex(map.get("sex")==null?null:map.get("sex").toString());
					user.setEmail(map.get("email")==null?null:map.get("email").toString());
					user.setAdress(map.get("adress")==null?null:map.get("adress").toString());
						}
					} catch (Exception e) {
					  throw new RuntimeException(e+"用户不匹配");	 
					}
					return user;
				}
		
		/*
		 * 查看已注册客户信息(刷新)
		 * 
		 */
		@SuppressWarnings("rawtypes")
		public Vector Look() {
			Vector<Vector<String>> str=new Vector<Vector<String>>();
			
			String sql="select *from hr_user";
			try {
				List<HashMap<String,Object>> maps=Test.select2(sql);
				if(maps!=null && maps.size()>0){
					for(HashMap<String,Object> map :maps) {
					Vector<String>  abc=new Vector<String>();
					
					abc.add(map.get("id")==null?null:map.get("id").toString());
					abc.add(map.get("name")==null?null:map.get("name").toString());
					abc.add(map.get("password")==null?null:map.get("password").toString());
					abc.add(map.get("age")==null?null:map.get("age").toString());
					abc.add(map.get("sex")==null?null:map.get("sex").toString());
					abc.add(map.get("tel")==null?null:map.get("tel").toString());
					abc.add(map.get("email")==null?null:map.get("email").toString());
					abc.add(map.get("adress")==null?null:map.get("adress").toString());
					abc.add(map.get("dept_id")==null?null:map.get("dept_id").toString());
					str.add(abc);
					}
					}
				
			} catch (Exception e) {
			  throw new RuntimeException(e+"用户不匹配");	 
			}
			return str;
		}
		/*
		 * 查看搜索注册用户信息
		 * 
		 */
		public Vector Look1(String name) {
			Vector<Vector<String>> str=new Vector<Vector<String>>();
			String name1="%"+name+"%";
			String sql="select *from hr_user where name like ?";	
			try {
				List<HashMap<String,Object>> maps=Test.select2(sql,name1);
				if(maps!=null && maps.size()>0){
					for(HashMap<String,Object> map :maps) {
					Vector<String>  abc=new Vector<String>();	
					abc.add(map.get("id")==null?null:map.get("id").toString());
					abc.add(map.get("name")==null?null:map.get("name").toString());
					abc.add(map.get("password")==null?null:map.get("password").toString());
					abc.add(map.get("age")==null?null:map.get("age").toString());
					abc.add(map.get("sex")==null?null:map.get("sex").toString());
					abc.add(map.get("email")==null?null:map.get("email").toString());
					abc.add(map.get("tel")==null?null:map.get("tel").toString());
					abc.add(map.get("adress")==null?null:map.get("adress").toString());	
					abc.add(map.get("dept_id")==null?null:map.get("dept_id").toString());
					str.add(abc);
					}
				}
			} catch (Exception e) {
			  throw new RuntimeException(e+"用户不匹配");	 
			}
			return str;
	}
		/*
		 * 
		 * 删除指定用户信息
		 */
		public void Delete(String id) {
			try {
				String sql="delete from hr_user where id=?";
				Test.ToUpdate(sql, id);
			} catch (Exception e) {
				// TODO: handle exception
				throw new RuntimeException(e+"用户不匹配");	 
			}
		}
		/*
		 * 修改指定用户信息
		 */
		public void UpDate(User user) {
			
			try {
				String sql="UpDate hr_user set name=?,password=?,age=?,sex=?,email=?,tel=?,adress=? where id=?";
				Test.ToUpdate(sql,user.getName(),user.getPassword(),user.getAge(),user.getSex(),user.getEmail(),user.getTel(),user.getAdress(),user.getId());
			} catch (Exception e) {
				// TODO: handle exception
				throw new RuntimeException(e+"用户不匹配");	
			}
			}
		public User Hui(String id){
			String sql="select *from hr_user where id=?";
			User user=null;	
			try {
				List<HashMap<String,Object>> maps=Test.select2(sql,id);
				if(maps!=null && maps.size()>0){
					HashMap<String,Object> map=maps.get(0);
					user =new User();
					user.setId(map.get("id")==null?null:map.get("id").toString());
					user.setName(map.get("name")==null?null:map.get("name").toString());
					user.setPassword(map.get("password")==null?null:map.get("password").toString());
					user.setAge(map.get("age")==null?null:map.get("age").toString());
					user.setSex(map.get("sex")==null?null:map.get("sex").toString());
					user.setEmail(map.get("email")==null?null:map.get("email").toString());	
					user.setTel(map.get("tel")==null?null:map.get("tel").toString());
					user.setAdress(map.get("adress")==null?null:map.get("adress").toString());
				}
			} catch (Exception e) {
			  throw new RuntimeException(e+"用户不匹配");	 
			}
			return user;
		}
		/*
		 * 多条件查询
		 */
		public Vector<Vector<String>> Multiple(String a,String b,String c){
			StringBuffer sql=new StringBuffer("select * from hr_user where 1=1");
			if(a!=null&&a.length()>0){
				sql.append(" and name like '"+a+"'");
		}
			if(b!=null&&b.length()>0){
				sql.append(" and dept_id='"+b+"'");
		}
			if(c!=null&&c.length()>0){
				sql.append(" and email='"+c+"'");
		}
			String sql1=sql.toString();
			Vector<Vector<String>> str=new Vector<Vector<String>>();	
			try {
				List<HashMap<String,Object>> maps=Test.select2(sql1);
				if(maps!=null && maps.size()>0){
					for(HashMap<String,Object> map :maps) {
					Vector<String>  abc=new Vector<String>();	
					abc.add(map.get("id")==null?null:map.get("id").toString());
					abc.add(map.get("name")==null?null:map.get("name").toString());
					abc.add(map.get("password")==null?null:map.get("password").toString());
					abc.add(map.get("age")==null?null:map.get("age").toString());
					abc.add(map.get("sex")==null?null:map.get("sex").toString());
					abc.add(map.get("email")==null?null:map.get("email").toString());
					abc.add(map.get("tel")==null?null:map.get("tel").toString());
					abc.add(map.get("adress")==null?null:map.get("adress").toString());
					abc.add(map.get("dept_id")==null?null:map.get("dept_id").toString());
					System.out.println(abc.toString());
					str.add(abc);
					}
				}
			} catch (Exception e) {
			  throw new RuntimeException(e+"用户不匹配");	 
			}
			return str;
			
		}
}
