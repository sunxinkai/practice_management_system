package edu.taru.office.view;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class MainView extends JFrame{
		public MainView(){
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\\u51EF\\Pictures\\flowers.jpg"));
		getContentPane().setBackground(Color.WHITE);
		setBackground(Color.WHITE);
		this.setTitle("人力管理系统");
		this.setSize(800, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menu = new JMenu("\u6743\u9650\u7BA1\u7406");
		menu.setFont(new Font("微软雅黑 Light", Font.PLAIN, 12));
		menuBar.add(menu);
		
		JMenuItem menuItem = new JMenuItem("个人信息");
		menu.add(menuItem);
		//查询个人信息
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				PersonalView bc=new PersonalView();
				bc.setSize(450,500);
				bc.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				bc.setVisible(true);
				bc.setLocationRelativeTo(null);
			}
		});
		
		JMenuItem menuItem_1 = new JMenuItem("切换用户");
		menu.add(menuItem_1);
		//切换用户事件
		menuItem_1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				LoginView str=new LoginView();
				MainView.this.dispose();
				str.setVisible(true);
			}
		});
		
		JMenuItem menuItem_2 = new JMenuItem("修改密码");
		menu.add(menuItem_2);
		menuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				PassView str=new PassView();
				MainView.this.dispose();
				str.setVisible(true);
			}
		});
		
		JMenuItem menuItem_7 = new JMenuItem("\u6CE8\u9500");
		menu.add(menuItem_7);
		
		JSeparator separator = new JSeparator();
		menu.add(separator);
		
		JMenuItem menuItem_3 = new JMenuItem("退出");
		menu.add(menuItem_3);
		//退出事件
		menuItem_3.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
			
		});
		
		JMenu menu_1 = new JMenu("\u7528\u6237\u7BA1\u7406");
		menu_1.setFont(new Font("微软雅黑", Font.PLAIN, 12));
		menuBar.add(menu_1);
		
		JMenuItem menuItem_4 = new JMenuItem("注册用户");
		menu_1.add(menuItem_4);
		
		//注册用户事件
		menuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				RegisterView bc=new RegisterView();
				bc.setSize(450,500);
				bc.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				bc.setVisible(true);
				bc.setLocationRelativeTo(null);
			}
		});
		
		JMenuItem menuItem_5 = new JMenuItem("查询用户");
		menu_1.add(menuItem_5);
		//查询用户事件
		menuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				UserView user=new UserView();
				user.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				user.setVisible(true);
			}
		});
		
		JMenu menu_2 = new JMenu("\u96C7\u5458\u7BA1\u7406");
		menu_2.setFont(new Font("微软雅黑", Font.PLAIN, 12));
		menuBar.add(menu_2);
		
		JMenuItem menuItem_6 = new JMenuItem("\u7BA1\u7406\u5458");
		menu_2.add(menuItem_6);
		getContentPane().setLayout(null);
	}
	public static void main(String []args) {
		@SuppressWarnings("unused")
		MainView main=new MainView();
	}
}
