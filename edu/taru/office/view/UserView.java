package edu.taru.office.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.BoxLayout;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import edu.taru.office.model.User;
import edu.taru.office.service.Management;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.Font;

@SuppressWarnings("serial")
public class UserView extends JFrame {
	static JTextField textField = new JTextField();
	static JTextField textField_1 = new JTextField();
	static JTextField textField_2 = new JTextField();
	String []header= {"���","����","����","����","�Ա�","�绰","����","��ַ","���ű��"};
	String [][]data=new String[50][9];
	private JTable table =new JTable(data,header);;
	JButton button_1 = new JButton("ˢ��");
	JButton button = new JButton("����");
	JScrollPane scrollPane = new JScrollPane();
	DefaultTableModel tab;
	public UserView() {
		this.setTitle("��������ϵͳ");
		this.setSize(603, 450);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "\u641C\u7D22", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 10, 567, 139);
		getContentPane().add(panel);
		panel.setLayout(null);
		textField.setFont(new Font("΢���ź�", Font.PLAIN, 12));
		
		
		textField.setBounds(73, 29, 144, 21);
		panel.add(textField);
		textField.setColumns(10);
		
		
		JLabel label = new JLabel("\u59D3 \u540D\uFF1A");
		label.setFont(new Font("΢���ź�", Font.PLAIN, 12));
		label.setBounds(10, 32, 54, 15);
		panel.add(label);
		button.setFont(new Font("΢���ź�", Font.PLAIN, 12));
	
		button.setBounds(274, 38, 91, 23);
		panel.add(button);
		//����
				button.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						Management str=new Management();
						String name="%"+textField.getText()+"%";
						String email=textField_1.getText();
						String dept=textField_2.getText();
						try{
							Vector<Vector<String>> rng1 =str.Multiple(name,email,dept);
							Vector str1=new Vector();	
							for(int i=0;i<header.length;i++) {
								str1.add(header[i]);
							}
							DefaultTableModel tab=new DefaultTableModel(rng1,str1);
							table.setModel(tab);
							table.updateUI();
//							scrollPane.setViewportView(table);
							}catch(Exception e) {
							JOptionPane.showMessageDialog(UserView.this,"����ʧ��");
						}
					}	
				});
		button_1.setFont(new Font("΢���ź�", Font.PLAIN, 12));
		
		button_1.setBounds(274, 86, 91, 23);
		panel.add(button_1);
		/*
		 * �޸�
		 */
		JButton button_2 = new JButton("\u4FEE\u6539");
		button_2.setFont(new Font("΢���ź�", Font.PLAIN, 12));
		button_2.setBounds(417, 38, 93, 23);
		panel.add(button_2);
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RevampView bc=new RevampView();
				Management management=new Management();
				User user2=management.Hui(UserView.textField.getText());
				bc.textField.setText(user2.getName());
				bc.textField_1.setText(user2.getPassword());
				if(user2.getSex().equals("��")){
					bc.radioButton.isSelected();
				}else{
					bc.radioButton_1.isSelected();
				}
				for(int i=0;i<bc.Dz.length;i++){
				if(user2.getAdress().equals(bc.Dz[i])){
					bc.comboBox.setSelectedIndex(i);
				}
				}
				bc.textField_2.setText(user2.getEmail());
				bc.textField_5.setText(user2.getAge());
				bc.textField_4.setText(user2.getTel());
				
				bc.setSize(450,500);
				bc.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				bc.setVisible(true);
				bc.setLocationRelativeTo(null);
			}
		});
		/*
		 * ɾ��
		 */
		JButton button_3 = new JButton("\u5220\u9664");
		button_3.setFont(new Font("΢���ź�", Font.PLAIN, 12));
		button_3.setBounds(417, 86, 93, 23);
		panel.add(button_3);
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String id=textField.getText();
				Management management=new Management();
				try {
					management.Delete(id);
					JOptionPane.showMessageDialog(UserView.this,"ɾ���ɹ�");
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(UserView.this,"ɾ��ʧ��");
				}
			}
		});
		JLabel label_1 = new JLabel("\u90E8 \u95E8\uFF1A");
		label_1.setFont(new Font("΢���ź�", Font.PLAIN, 12));
		label_1.setBounds(10, 68, 54, 15);
		panel.add(label_1);
		textField_1.setFont(new Font("΢���ź�", Font.PLAIN, 12));
		
		
		textField_1.setBounds(73, 60, 144, 21);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel label_2 = new JLabel("\u90AE \u7BB1\uFF1A");
		label_2.setFont(new Font("΢���ź�", Font.PLAIN, 12));
		label_2.setBounds(10, 94, 54, 15);
		panel.add(label_2);
		
		textField_2 = new JTextField();
		textField_2.setBounds(73, 91, 144, 21);
		panel.add(textField_2);
		textField_2.setColumns(10);
		//��ѯ����
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				Management syr=new Management();
				try{
					Vector<Vector<String>> str=syr.Look();
					Vector str1=new Vector();	
					for(int i=0;i<header.length;i++) {
						str1.add(header[i]);
					}
					tab=new DefaultTableModel(str,str1);
					table.setModel(tab);
					table.updateUI();
					}catch(Exception e) {
					JOptionPane.showMessageDialog(UserView.this,"��ѯʧ��");
				}
				
			}
		});
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "\u6570\u636E\u533A\u57DF", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 159, 567, 209);
		getContentPane().add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		panel_1.add(scrollPane);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
						int k=table.rowAtPoint(e.getPoint());
						Vector<Vector<String>> str=tab.getDataVector();
						Vector str1=str.get(k);
						RevampView bc=new RevampView();
						textField.setText(str1.get(0).toString());
						bc.textField.setText(str1.get(1).toString());
						bc.textField_1.setText(str1.get(2).toString());
						bc.textField_2.setText(str1.get(6).toString());
						if(str1.get(4).toString().equals("��")){
							bc.radioButton.isSelected();
						}else{
							bc.radioButton_1.isSelected();
						}
						bc.textField_4.setText(str1.get(5).toString());
						bc.textField_5.setText(str1.get(3).toString());
						for(int i=0;i<bc.Dz.length;i++){
						if(str1.get(7).toString().toString().equals(bc.Dz[i])){
							bc.comboBox.setSelectedIndex(i);
						}
						}
						bc.setSize(450,500);
						bc.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						bc.setVisible(true);
						bc.setLocationRelativeTo(null);
			}
		});
		scrollPane.setViewportView(table);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UserView user=new UserView();
	}
}
