package edu.taru.office.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import edu.taru.office.model.User;
import edu.taru.office.service.Management;

import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

public class RevampView extends JFrame{
	static JTextField textField = new JTextField();
	static JTextField textField_1=new JTextField();
	static JTextField textField_2=new JTextField();;
	static JTextField textField_3;
	static JTextField textField_4;
	static JTextField textField_5;
	final JRadioButton radioButton;
	final JRadioButton radioButton_1;
	final String[] Dz= {"河南","山东","湖南","山西","湖北","新疆","内蒙古","北京"};
	final JComboBox comboBox = new JComboBox(Dz);
	
	public RevampView() {
		this.setTitle("修改用户");
		this.setSize(416, 395);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u7528\u6237\u540D\uFF1A");
		lblNewLabel.setFont(new Font("微软雅黑", Font.PLAIN, 14));
		lblNewLabel.setBounds(27, 22, 63, 25);
		getContentPane().add(lblNewLabel);
		
		
		textField.setBounds(100, 23, 167, 25);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel label = new JLabel("密  码：");
		label.setFont(new Font("微软雅黑", Font.PLAIN, 14));
		label.setBounds(27, 68, 54, 25);
		getContentPane().add(label);
		

		textField_1.setBounds(100, 69, 167, 25);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("\u6027  \u522B\uFF1A");
		lblNewLabel_1.setFont(new Font("微软雅黑", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(27, 174, 54, 15);
		getContentPane().add(lblNewLabel_1);
		
		radioButton= new JRadioButton("\u7537");
		radioButton.setBounds(100, 171, 42, 23);
		getContentPane().add(radioButton);
		
		radioButton_1= new JRadioButton("\u5973");
		//逻辑组件
		ButtonGroup bg=new ButtonGroup();
		bg.add(radioButton);
		bg.add(radioButton_1);
		radioButton_1.setBounds(184, 171, 48, 23);
		getContentPane().add(radioButton_1);
		
		JLabel label_1 = new JLabel("\u90AE  \u7BB1\uFF1A");
		label_1.setFont(new Font("微软雅黑", Font.PLAIN, 14));
		label_1.setBounds(27, 239, 54, 17);
		getContentPane().add(label_1);
		
		textField_2 = new JTextField();
		textField_2.setBounds(99, 238, 168, 21);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JLabel label_2 = new JLabel("籍  贯：");
		label_2.setFont(new Font("微软雅黑", Font.PLAIN, 14));
		label_2.setBounds(27, 125, 54, 15);
		getContentPane().add(label_2);
		
		
		comboBox.setBounds(99, 123, 68, 21);
		getContentPane().add(comboBox);
		
		textField_4 = new JTextField();
		textField_4.setBounds(100, 269, 167, 21);
		getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		JLabel label_3 = new JLabel("\u5E74  \u9F84\uFF1A");
		label_3.setFont(new Font("微软雅黑", Font.PLAIN, 14));
		label_3.setBounds(27, 209, 54, 15);
		getContentPane().add(label_3);
		
		textField_5 = new JTextField();
		textField_5.setBounds(100, 207, 167, 21);
		getContentPane().add(textField_5);
		textField_5.setColumns(10);
		
		JButton button = new JButton("修改");
		button.setFont(new Font("微软雅黑", Font.PLAIN, 14));
		button.setBounds(77, 312, 93, 23);
		getContentPane().add(button);
		//修改信息传递
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				//调用登录里面注册方法
				Management management=new Management();
				String username=textField.getText();//姓名
				String password=textField_1.getText();//密码
				String sex="男";//性别
				if(radioButton_1.isSelected()) {
					sex="女";
				}
				int index=comboBox.getSelectedIndex();//籍贯
				String Dx=Dz[index];
				String em=textField_2.getText();//邮箱
				String age=textField_5.getText();//年龄
				String tel=textField_4.getText();//电话
				
				//调用用户数据，把参数赋值给用户成员变量
				User user=new User();
				user.setName(username);
				user.setPassword(password);
				user.setSex(sex);
				user.setEmail(em);
				user.setAdress(Dx);
				user.setAge(age);
				user.setTel(tel);
				user.setId(UserView.textField.getText());
				try {
					management.UpDate(user);
					JOptionPane.showMessageDialog(RevampView.this,"修改成功");
				}catch(Exception ex) {
					JOptionPane.showMessageDialog(RevampView.this,"修改失败");
				}
			}
		});
		
		JButton button_1 = new JButton("重置");
		button_1.setFont(new Font("微软雅黑", Font.PLAIN, 14));
		button_1.setBounds(225, 312, 93, 23);
		getContentPane().add(button_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u7535  \u8BDD\uFF1A");
		lblNewLabel_2.setFont(new Font("微软雅黑", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(27, 266, 54, 15);
		getContentPane().add(lblNewLabel_2);
		
		
		//重置问题
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RevampView.this.dispose();
				new RegisterView().setVisible(true);

			}
		});
		
	}

	public static void main(String[] args) {
		RevampView abc=new RevampView(); 
	}
}

