package edu.taru.office.view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Expression;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import edu.taru.office.model.User;
import edu.taru.office.service.Management;

import javax.swing.JPanel;
import javax.swing.ImageIcon;

public class LoginView extends JFrame{
	
	JButton loginBtn=new JButton("登录");
	JButton exitBtn=new JButton("退出");
	static JTextField loginText=new JTextField();
	static JPasswordField passText=new JPasswordField(20);
	JLabel jLable_1=new JLabel("用户名：");
	JLabel jLable_2=new JLabel("密  码：");
	
	
	public LoginView(){
	getContentPane().setLayout(null);
	loginBtn.setBounds(300, 200, 70, 35);
	getContentPane().add(loginBtn);
	exitBtn.setBounds(300, 250, 70, 35);
	getContentPane().add(exitBtn);
	jLable_1.setBounds(20, 200, 70, 35);
	getContentPane().add(jLable_1);
	jLable_2.setBounds(20, 250, 70, 35);
	getContentPane().add(jLable_2);
	loginText.setBounds(90, 200, 180, 35);
	getContentPane().add(loginText);
	passText.setBounds(90, 250, 180, 35);
	getContentPane().add(passText);
	
	JLabel lblNewLabel = new JLabel("");
	lblNewLabel.setIcon(new ImageIcon(LoginView.class.getResource("/edu/taru/office/photo/surf_1.jpg")));
	lblNewLabel.setBounds(0, 0, 384, 197);
	getContentPane().add(lblNewLabel);
	
	exitBtn.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {
			// TODO 自动生成方法存根
			System.exit(0);
		}
		
	});
	loginBtn.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {
			// TODO 自动生成方法存根
			//获取用户名
			String username=loginText.getText();
			//获取密码
			String password=new String(passText.getPassword());
			Management ren=new Management();
			User user=ren.login(username,password);
			try {
				if(user!=null) {
					MainView main=new MainView();
					LoginView.this.dispose();
					main.setVisible(true);
				}else {
					JOptionPane.showMessageDialog(LoginView.this, "登陆失败");
				}
			} catch (Exception e) {
				// TODO: handle exception
				
			}
		}
	});
	this.setTitle("人力管理系统");
	this.setSize(400, 350);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setVisible(true);
	this.setLocationRelativeTo(null);
	}
	public static void main(String []args){
		LoginView btn=new LoginView();
	}
}
