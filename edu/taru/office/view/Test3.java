package edu.taru.office.view;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;

@SuppressWarnings("unused")
public class Test3 {

	public static void main(String[] args) {
		BufferedReader reader =null;
		BufferedWriter writer =null;
		try {
			reader =new BufferedReader(new FileReader("D:\\xing.txt"));
			writer=new BufferedWriter(new FileWriter("D:\\sun.txt"));
			while(true) {
			String str=reader.readLine();
				if(str==null) {
					break;
				}
				writer.write(str);
				writer.newLine();
				writer.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(reader!=null) {
				try {
					reader.close();
				} catch (Exception e2) {
					e2.getStackTrace();
				}
				
			}
			if(writer!=null) {
				try {
					writer.close();
				} catch (Exception e3) {
					e3.getStackTrace();
				}
			}
		}

	}

}
